<?php

namespace Traqy\EasyCore\Helpers;

class Helper {

    public static function getmodules() {
        $modules = config('easyCore.system_modules');
        $count = 1;
        $collection = array();

        foreach ($modules as $module) {
            $collection[] = (object) ['id' => $module, 'name' => $module];
            $count ++;
        }

        return collect($collection);
    }

    public static function getPermissionTypes() {
        $permissions = [
            'perm_add' => 'Add',
            'perm_edit' => 'Edit',
            'perm_view' => 'View',
            'perm_delete' => 'Delete',
        ];

        $collection = array();
        foreach ($permissions as $key => $permission) {
            $collection[] = (object) ['id' => $key, 'name' => $permission];
        }

        return collect($collection);
    }

    public static function isAllowed($userID, $moduleName, $type) {

        $roleRepository = app()->make(\Traqy\EasyCore\Interfaces\Repositories\RoleRepositoryInterface::class);
        $adminRoleObj = $roleRepository->findBy('name', config('easyCore.default_user'), array('id'));

        $userRoleRepository = app()->make(\Traqy\EasyCore\Interfaces\Repositories\UserRoleRepositoryInterface::class);
        $assignedRoles = $userRoleRepository->assignedRoles($userID);

        $rolePermissionRepository = app()->make(\Traqy\EasyCore\Interfaces\Repositories\RolePermissionRepositoryInterface::class);

        if (!empty($assignedRoles)) {
            if ($adminRoleObj != null) {

                if (in_array($adminRoleObj->id, $assignedRoles)) {
                    return true;
                } else {

                    foreach ($assignedRoles as $roleID) {
                        $permissionObj = $rolePermissionRepository->getByModule($roleID, $moduleName);
                        if ($permissionObj != null) {

                            if ($permissionObj->{$type}) {
                                return true;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

}
