<?php

namespace Traqy\EasyCore\Interfaces\Repositories;

interface UserRoleRepositoryInterface {

    public function assignedRoles($userID);

    public function deleteByUser($userId);
}
