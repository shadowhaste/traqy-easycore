<?php

namespace Traqy\EasyCore\Interfaces;

interface RepositoryInterface {

    public function create(array $data);

    public function update(array $data, $id, $attribute = "id");

    public function read($id);

    public function delete($id, $route = null);

    public function paginate($perPage = 15, $columns = array('*'));

    public function find($id, $columns = array('*'));

    public function findBy($attribute, $value, $columns = array('*'));

    public function findOrderBy($attribute, $value, $columns = array('*'), $orderByField = 'id', $orderBy = 'desc');

    public function findAllBy($attribute, $value, $columns = array('*'));

    public function findAllByOrderBy($attribute, $value, $columns = array('*'), $orderByField = 'id', $orderBy = 'desc');

    public function getAll($columns = array('*'));

    public function getAllOrderBy($value = 'updated_at', $desc = 'desc', $columns = array('*'));

    public function getLatestData();

    public function load($id, $columns = array('*'));

    public function loadArray($ids, $columns = array('*'));
}
