<?php

namespace Traqy\EasyCore\Interfaces;

interface FactoryInterface{
	public static function make( $type, $args = [] );
}