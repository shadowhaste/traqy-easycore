<?php
namespace Traqy\EasyCore\Interfaces;

use Illuminate\Http\Request;

interface DatatableInterface
{
	public function records(Request $request);
	public function displayRecords($users, $request, $totalData, $totalFiltered);

}

