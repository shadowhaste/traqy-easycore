<?php

namespace Traqy\EasyCore\Interfaces;

interface UserRoleServiceInterface extends ServiceInterface {

    public function getRoles($userID);

    public function deleteByUser($userId);

    public function assignRoles($userId, $roles);
}
