<?php

namespace Traqy\EasyCore\Interfaces;

use Traqy\EasyCore\Interfaces\ServiceInterface;

interface RoleServiceInterface extends ServiceInterface {

    public function choices();
}
