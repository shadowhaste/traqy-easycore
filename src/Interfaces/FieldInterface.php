<?php

namespace Traqy\EasyCore\Interfaces;

interface FieldInterface{

	public function getKey();
	public function getValue();
	public function getLabel();

}