<?php

namespace Traqy\EasyCore\Interfaces;

use Traqy\EasyCore\Interfaces\RepositoryInterface;

interface GuiServiceInterface {

    public function addField($type, $args);

    public function setModel(RepositoryInterface $field);
}
