<?php

namespace Traqy\EasyCore\Seeds;

use Illuminate\Database\Seeder;
use Traqy\EasyCore\Models\User;
use Traqy\EasyCore\Models\Role;
use Traqy\EasyCore\Models\RolePermission;
use Traqy\EasyCore\Models\UserRole;

class UserTableSeeder extends Seeder {

    public function run() {
        $userObj = User::create(array(
                    'name' => 'test',
                    'email' => 'ariestraquena@gmail.com',
                    'password' => bcrypt('test'),
        ));


        $roleObj = Role::create(array(
                    'name' => config("easyCore.default_user"),
        ));

        //add permissions to role
        //get all modules
        $modules = config('easyCore.system_modules');
        foreach ($modules as $module) {

            RolePermission::create(array(
                'role_id' => $roleObj->id,
                'perm_add' => 1,
                'perm_edit' => 1,
                'perm_view' => 1,
                'perm_delete' => 1,
                'module_name' => $module
            ));
        }

        //add role to user
        UserRole::create(array(
            'user_id' => $userObj->id,
            'role_id' => $roleObj->id
        ));
    }

}
