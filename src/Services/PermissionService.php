<?php

namespace Traqy\EasyCore\Services;

use Traqy\EasyCore\Interfaces\Repositories\RolePermissionRepositoryInterface;
use Traqy\EasyCore\Interfaces\PermissionServiceInterface;

class PermissionService extends CoreService implements PermissionServiceInterface {

    public function __construct(RolePermissionRepositoryInterface $repository) {
        $this->repository = $repository;
    }

}
