<?php

namespace Traqy\EasyCore\Services;

use Traqy\EasyCore\Interfaces\UserRoleServiceInterface;
use Traqy\EasyCore\Interfaces\Repositories\UserRoleRepositoryInterface;

class UserRoleService extends CoreService implements UserRoleServiceInterface {

    public function __construct(UserRoleRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function getRoles($userID) {
        return $this->repository->assignedRoles($userID);
    }

    public function deleteByUser($userId) {
        $this->repository->deleteByUser($userId);
    }

    public function assignRoles($userId, $roles) {
        //remove first the assigned roles
        $this->deleteByUser($userId);

        //add new roles
        if (!empty($roles)) {
            foreach ($roles as $roleID) {
                $data = ['user_id' => $userId, 'role_id' => $roleID];
                $this->create($data);
            }
        }
    }

}
