<?php

namespace Traqy\EasyCore\Services;

use Traqy\EasyCore\Interfaces\UserServiceInterface;
use Traqy\EasyCore\Interfaces\Repositories\UserRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserService extends CoreService implements UserServiceInterface {

    public function __construct(UserRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function updatePassword(Request $request) {
        $user = $this->read(auth()->user()->id);

        $checkPassword = Hash::check($request->oldpassword, $user->password);
        if (!$checkPassword) {
            return (object) [
                        'status' => 400,
                        'message' => __('messages.settings.password.incorrect'),
            ];
        }

        if ($request->password !== $request->confirm_password) {
            return (object) [
                        'status' => 400,
                        'message' => __('messages.settings.password.notmatch'),
            ];
        }

        //update
        $update = $this->update([
            "password" => bcrypt($request->password)
                ], $user->id);

        if ($update) {
            return (object) [
                        'status' => 200,
                        'message' => __('messages.settings.password.updated'),
            ];
        }
    }

}
