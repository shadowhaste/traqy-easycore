<?php

namespace Traqy\EasyCore\Services;

use Traqy\EasyCore\Interfaces\ServiceInterface;
use Traqy\EasyCore\Interfaces\RepositoryInterface;

abstract class CoreService implements ServiceInterface {

    //crud
    protected $repository;

    public function __construct(RepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function create(array $data) {
        return $this->repository->create($data);
    }

    public function update(array $data, $id, $attribute = "id") {
        return $this->repository->update($data, $id, $attribute);
    }

    public function read($id) {
        return $this->repository->read($id);
    }

    public function delete($id, $route = null) {
        return $this->repository->delete($id);
    }

    public function getRepository() {
        return $this->repository;
    }

    public function paginate($perPage = 15, $columns = array('*')) {
        return $this->repository->paginate($perPage, $columns);
    }

    public function find($id, $columns = array('*')) {
        return $this->repository->find($id, $columns);
    }

    public function findBy($attribute, $value, $columns = array('*')) {
        return $this->repository->findBy($attribute, $value, $columns);
    }

    public function findOrderBy($attribute, $value, $columns = array('*'), $orderByField = 'id', $orderBy = 'desc') {
        return $this->repository->findOrderBy($attribute, $value, $columns, $orderByField, $orderBy);
    }

    public function findAllBy($attribute, $value, $columns = array('*')) {
        return $this->repository->findAllBy($attribute, $value, $columns);
    }

    public function findAllByOrderBy($attribute, $value, $columns = array('*'), $orderByField = 'id', $orderBy = 'desc') {
        return $this->repository->findAllByOrderBy($attribute, $value, $columns, $orderByField, $orderBy);
    }

    public function getAll($columns = array('*')) {
        return $this->repository->getAll($columns);
    }

    public function getAllOrderBy($value = 'updated_at', $desc = 'desc', $columns = array('*')) {
        return $this->repository->getAllOrderBy($value, $desc, $columns);
    }

    public function getLatestData() {
        return $this->repository->getLatestData();
    }

    public function load($id, $columns = array('*')) {
        return $this->loadArray([$id], $columns);
    }

    public function loadArray($ids, $columns = array('*')) {
        return $this->repository->loadArray($ids, $columns);
    }

    public function searchDatatableByValue($start, $limit, $order, $dir, $columns = array('*')) {
        return $this->repository->searchDatatableByValue($start, $limit, $order, $dir, $columns);
    }

}
