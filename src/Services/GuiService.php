<?php

namespace Traqy\EasyCore\Services;

use Traqy\EasyCore\Factories\FieldFactory;
use Traqy\EasyCore\Interfaces\GuiServiceInterface;
use Traqy\EasyCore\Interfaces\RepositoryInterface;

class GuiService implements GuiServiceInterface {

    protected $route = false;
    protected $title;
    public $fields = [];
    public $columns = [];
    protected $recordRoute;

    public function addColumn($label, $field, $width = "") {
        $this->columns[] = (object) [
                    "label" => $label,
                    "field" => $field,
                    "width" => $width,
        ];
    }

    public function getColumnFields($withOptions = true) {

        $columns = array();
        foreach ($this->columns as $column) {
            $columns[] = $column->field;
        }

        if ($withOptions) {

            $columns[] = "options";
        }
        return $columns;
    }

    public function getColumnLabels($withOptions = true) {

        $columns = array();
        foreach ($this->columns as $column) {
            $columns[] = $column->label;
        }

        if ($withOptions) {

            $columns[] = "Options";
        }
        return $columns;
    }

    public function getDatatableColumns($withOptions = true) {

        $columns = array();
        foreach ($this->columns as $column) {
            $data = (object) [
                        "data" => $column->field,
            ];

            $columns[] = $data;
        }

        if ($withOptions) {
            $data = (object) [
                        "data" => "options",
            ];

            $columns[] = $data;
        }


        return $columns;
    }

    public function addField($type, $args) {
        $this->fields[] = FieldFactory::make($type, (object) $args);
    }

    public function setModel(RepositoryInterface $model) {
        $this->model = $model;
    }

    public function setRoute($route) {
        $this->route = $route;
    }

    public function setRecordRoute($route) {
        $this->recordRoute = $route;
    }

    public function getRecordRoute() {
        return $this->recordRoute;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function getTitle() {
        return $this->title;
    }

    public function route($page, $id = false) {
        switch ($page) {
            case "list":
                $query = http_build_query(request()->query());
                return "/{$this->route}" . ( $query ? "?{$query}" : "" );
            case "create": return "/{$this->route}/create";
            case "edit": return "/{$this->route}/{$id}/edit";
            case "store": return "/{$this->route}";
            case "update": return "/{$this->route}/{$id}";
            case "delete": return "/{$this->route}/{$id}";
        }
    }

    public function message($module) {
        $message = trans("easycore.crud.{$module}");
        return str_replace("%item%", $this->title, $message);
    }

}
