<?php

namespace Traqy\EasyCore\Services;

use Traqy\EasyCore\Interfaces\RoleServiceInterface;
use Traqy\EasyCore\Interfaces\Repositories\RoleRepositoryInterface;

class RoleService extends CoreService implements RoleServiceInterface {

    public function __construct(RoleRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function choices() {
        return $this->repository->choices();
    }

}
