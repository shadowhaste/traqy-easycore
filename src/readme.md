### Status Codes
* `1` - Include easy core package to laravel project
* `2` - Run migration (php artisan migrate)
* `3` - Run seeds [ php artisan db:seed --class=Traqy\\EasyCore\\Seeds\\DatabaseSeeder ]
* `4` - Do a vendor publish once (php artisan vendor:publish --force)
* `5` - Compile npm (npm run watch)
* `6` - If error occurs about yarn do a npm install --global yarn
