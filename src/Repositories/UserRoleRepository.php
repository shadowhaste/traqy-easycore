<?php

namespace Traqy\EasyCore\Repositories;

use Traqy\EasyCore\Interfaces\Repositories\UserRoleRepositoryInterface as RepositoryInterface;
use Traqy\EasyCore\Interfaces\Models\UserRoleInterface;

class UserRoleRepository extends CoreRepository implements RepositoryInterface {

    public function __construct(UserRoleInterface $model) {
        parent::__construct($model);
        $this->model = $model;
    }

    public function assignedRoles($userID) {
        $roles = $this->model->where('user_id', $userID)->pluck('role_id')->toArray();
        return $roles;
    }

    public function deleteByUser($userId) {
        return $this->model->where('user_id', $userId)->delete();
    }

}
