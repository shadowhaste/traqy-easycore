<?php

namespace Traqy\EasyCore\Repositories;

use Traqy\EasyCore\Interfaces\RepositoryInterface;
use Traqy\EasyCore\Interfaces\Models\ModelInterface;

class CoreRepository implements RepositoryInterface {

    protected $model;

    public function __construct(ModelInterface $model) {
        $this->model = $model;
    }

    public function create(array $data) {
        return $this->model->create($data);
    }

    public function update(array $data, $id, $attribute = "id") {
        return $this->model->where($attribute, '=', $id)->update($data);
    }

    public function updateByFields($fields, $id, $request) {
        $data = $this->read($id);
        foreach ($fields as $item) {
            $key = $item->getKey();
            $data->$key = $item->input($request);
        }

        $data->save();

        return $data;
    }

    public function read($id) {
        return $this->model::find($id);
    }

    public function delete($id, $route = null) {
        $data = $this->read($id);
        return $data->delete();
    }

    public function paginate($perPage = 15, $columns = array('*')) {
        return $this->model->paginate($perPage, $columns);
    }

    public function find($id, $columns = array('*')) {
        return $this->model->find($id, $columns);
    }

    public function findBy($attribute, $value, $columns = array('*')) {
        return $this->model->where($attribute, '=', $value)->first($columns);
    }

    public function findOrderBy($attribute, $value, $columns = array('*'), $orderByField = 'id', $orderBy = 'desc') {
        return $this->model->where($attribute, '=', $value)
                        ->orderBy($orderByField, $orderBy)
                        ->first($columns);
    }

    public function findAllBy($attribute, $value, $columns = array('*')) {
        return $this->model->where($attribute, '=', $value)->get($columns);
    }

    public function findAllByOrderBy($attribute, $value, $columns = array('*'), $orderByField = 'id', $orderBy = 'desc') {
        return $this->model->where($attribute, '=', $value)
                        ->orderBy($orderByField, $orderBy)
                        ->get($columns);
    }

    public function getAll($columns = array('*')) {
        return $this->model->get($columns);
    }

    public function getAllOrderBy($value = 'updated_at', $desc = 'desc', $columns = array('*')) {
        return $this->model->orderBy($value, $desc)->get($columns);
    }

    public function getLatestData() {
        $obj = $this->model->latest()->first();
        return $obj;
    }

    public function load($id, $columns = array('*')) {
        return $this->loadArray([$id], $columns);
    }

    public function loadArray($ids, $columns = array('*')) {
        return $this->model->whereIn('id', $ids)->get($columns);
    }

    public function searchDatatableByValue($start, $limit, $order, $dir, $columns = array('*')) {
        return $this->model->offset($start)
                        ->limit($limit)
                        ->orderBy($order, $dir)
                        ->get($columns);
    }

}
