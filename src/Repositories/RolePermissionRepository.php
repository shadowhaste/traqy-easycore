<?php

namespace Traqy\EasyCore\Repositories;

use Traqy\EasyCore\Interfaces\Repositories\RolePermissionRepositoryInterface as RepositoryInterface;
use Traqy\EasyCore\Interfaces\Models\RolePermissionInterface;

class RolePermissionRepository extends CoreRepository implements RepositoryInterface
{
	public function __construct(RolePermissionInterface $model){
		parent::__construct( $model );
		$this->model = $model;
	}

	public function searchDatatable($search, $start, $limit, $order, $dir, $columns = array('*'), $roleID = null){
		 return $this->model->where('role_id', $roleID)
							->where('id','LIKE',"%{$search}%")
                            ->orWhere('role_id', 'LIKE',"%{$search}%")
							->orWhere('module_name', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get($columns);

	}

	public function searchDatatableCount($search, $roleID = null){
		return $this->model->where('role_id', $roleID)
							->where('id','LIKE',"%{$search}%")
							->orWhere('role_id', 'LIKE',"%{$search}%")
							->orWhere('module_name', 'LIKE',"%{$search}%")
							->count();

	}

	public function searchDatatableByValue($start, $limit, $order, $dir, $columns = array('*'), $roleID = null){
		return $this->model->where('role_id', $roleID)
						->offset($start)
						->limit($limit)
						->orderBy($order,$dir)
						->get($columns);
	}

	public function getByModule($roleID, $module){
		return $this->model
				->where('role_id', $roleID)
				->where('module_name', $module)
				->first();
				
	}

}
