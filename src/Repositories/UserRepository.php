<?php

namespace Traqy\EasyCore\Repositories;

use Traqy\EasyCore\Interfaces\Repositories\UserRepositoryInterface as RepositoryInterface;
use Traqy\EasyCore\Interfaces\Models\UserInterface;

class UserRepository extends CoreRepository implements RepositoryInterface {

    public function __construct(UserInterface $model) {
        parent::__construct($model);
        $this->model = $model;
    }

    public function searchUserDatatable($id, $search, $start, $limit, $order, $dir, $columns = array('*')) {
        return $this->model
                        ->where('id', '!=', $id)
                        ->where('id', 'LIKE', "%{$search}%")
                        ->orWhere('name', 'LIKE', "%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order, $dir)
                        ->get($columns);
    }

    public function searchUserDatatableCount($id, $search) {
        return $this->model
                        ->where('id', '!=', $id)
                        ->where('id', 'LIKE', "%{$search}%")
                        ->orWhere('name', 'LIKE', "%{$search}%")
                        ->count();
    }

    public function searchUserDatatableByValue($id, $start, $limit, $order, $dir, $columns = array('*')) {
        return $this->model
                        ->where('id', '!=', $id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order, $dir)
                        ->get($columns);
    }

    public function getUserByEmail($email) {
        return $this->model->where('email', $email)->first();
    }

    public function getUsersExcludeCurrent($id, $columns = array('*')) {
        return $this->model
                        ->where('id', '!=', $id)
                        ->get($columns);
    }

}
