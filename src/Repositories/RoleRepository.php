<?php

namespace Traqy\EasyCore\Repositories;

use Traqy\EasyCore\Interfaces\Repositories\RoleRepositoryInterface as RepositoryInterface;
use Traqy\EasyCore\Interfaces\Models\RoleInterface;

class RoleRepository extends CoreRepository implements RepositoryInterface {

    public function __construct(RoleInterface $model) {
        parent::__construct($model);
        $this->model = $model;
    }

    public function searchDatatable($search, $start, $limit, $order, $dir, $columns = array('*')) {
        return $this->model->where('id', 'LIKE', "%{$search}%")
                        ->orWhere('name', 'LIKE', "%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order, $dir)
                        ->get($columns);
    }

    public function searchDatatableCount($search) {
        return $this->model->where('id', 'LIKE', "%{$search}%")
                        ->orWhere('name', 'LIKE', "%{$search}%")
                        ->count();
    }

    public function choices() {
        return $this->model->orderBy("id")->select("id", "name")->get();
    }

}
