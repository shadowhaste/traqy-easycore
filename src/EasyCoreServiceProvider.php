<?php

namespace Traqy\EasyCore;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class EasyCoreServiceProvider extends ServiceProvider {

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot() {
        $this->loadMigrationsFrom(__DIR__ . '/Migrations');
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
        $this->loadViewsFrom(__DIR__ . "/Resources/views", "easyCore");

        // Copy Assets to the public folder //
        $this->publishes([
            __DIR__ . "/Resources/assets" => public_path("vendor/easycore"),
            __DIR__ . "/Config/easyCore.php" => config_path("easyCore.php"),
            __DIR__ . "/Resources/app/js" => resource_path("assets/js"),
                ], "assets");

        // Copy Views to the resource folder //
        $this->publishes([__DIR__ . '/Resources/views' => resource_path('views/vendor/easycore')], "views");

        // Copy Language File //
        $this->publishes([__DIR__ . '/Languages/' => resource_path('lang/en'),]);

        Validator::extend('is_module_exist', function ($attribute, $value, $parameters, $validator) {

            $module = Models\RolePermission::where('role_id', request()->role_id)
                    ->where('module_name', request()->module_name)
                    ->first();

            if ($module != null) {
                return false;
            }

            return true;
        });

        Validator::extend('unique_user_on_update', function ($attribute, $value, $parameters, $validator) {

            \Illuminate\Support\Facades\Log::Debug($value);

            return true;
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register() {
        #main
        $this->app->bind(Interfaces\GuiServiceInterface::class, Services\GuiService::class);
        $this->app->bind(Interfaces\ServiceInterface::class, Services\CoreService::class);

        #services
        $this->app->bind(Interfaces\UserServiceInterface::class, Services\UserService::class);
        $this->app->bind(Interfaces\RoleServiceInterface::class, Services\RoleService::class);
        $this->app->bind(Interfaces\PermissionServiceInterface ::class, Services\PermissionService::class);
        $this->app->bind(Interfaces\UserRoleServiceInterface::class, Services\UserRoleService::class);

        #repositories
        $this->app->bind(Interfaces\RepositoryInterface::class, Repositories\CoreRepository::class);
        $this->app->bind(Interfaces\Repositories\UserRepositoryInterface::class, Repositories\UserRepository::class);
        $this->app->bind(Interfaces\Repositories\RoleRepositoryInterface::class, Repositories\RoleRepository::class);
        $this->app->bind(Interfaces\Repositories\RolePermissionRepositoryInterface::class, Repositories\RolePermissionRepository::class);
        $this->app->bind(Interfaces\Repositories\UserRoleRepositoryInterface::class, Repositories\UserRoleRepository::class);

        #models
        $this->app->bind(Interfaces\Models\UserInterface::class, Models\User::class);
        $this->app->bind(Interfaces\Models\RoleInterface::class, Models\Role::class);
        $this->app->bind(Interfaces\Models\RolePermissionInterface::class, Models\RolePermission::class);
        $this->app->bind(Interfaces\Models\UserRoleInterface::class, Models\UserRole::class);
    }

}
