<?php

namespace Traqy\EasyCore\Mailables;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordMail extends Mailable {

    use Queueable,
        SerializesModels;

    protected $email;
    protected $password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $password) {
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->view('easyCore::mailable.password')
                        ->subject("Dashboard Access Details")
                        ->from(config("easyCore.system.email"))
                        ->with([
                            "email" => $this->email,
                            "password" => $this->password
        ]);
    }

}
