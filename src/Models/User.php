<?php

namespace Traqy\EasyCore\Models;

use Traqy\EasyCore\Interfaces\Models\UserInterface;

class User extends Core implements UserInterface {

    protected $fillable = [
        'name',
        'email',
        'password',
    ];
    protected $hidden = [
        'remember_token',
    ];

    protected static function boot() {
        parent::boot();
        static::creating(function ($model) {
            $model->generatePassword();
        });
    }

    public function generatePassword() {
        $password = str_random(10);
        $this->attributes['password'] = bcrypt($password);
        \Illuminate\Support\Facades\Log::Debug($password);
        // Email the password //
//        Mail::to($this->email)->send(
//                new PasswordMail(
//                $this->email, $password
//                )
//        );
        // End Email the password //
    }

}
