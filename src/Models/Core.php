<?php

namespace Traqy\EasyCore\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Core extends Model
{
    use SoftDeletes;
	protected $dates = ['deleted_at'];
}
