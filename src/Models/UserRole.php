<?php

namespace Traqy\EasyCore\Models;

use Illuminate\Database\Eloquent\Model;
use Traqy\EasyCore\Interfaces\Models\UserRoleInterface;

class UserRole extends Model implements UserRoleInterface {

    protected $fillable = [
        'user_id',
        'role_id'
    ];

}
