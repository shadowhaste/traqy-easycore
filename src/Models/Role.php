<?php

namespace Traqy\EasyCore\Models;

use Traqy\EasyCore\Interfaces\Models\RoleInterface;

class Role extends Core implements RoleInterface {

    protected $fillable = [
        'name',
    ];

}
