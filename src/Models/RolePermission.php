<?php

namespace Traqy\EasyCore\Models;
use Traqy\EasyCore\Interfaces\Models\RolePermissionInterface;

class RolePermission extends Core implements RolePermissionInterface
{
	protected $fillable = [
		'role_id',
		'perm_add',
		'perm_edit',
		'perm_view',
		'perm_delete',
		'module_name'
    ];
}
