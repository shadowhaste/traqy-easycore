<?php

namespace Traqy\EasyCore\Jobs;

use Illuminate\Support\Facades\Log;

class sampleJob extends Job {

    /**
     * Create a new job instance.
     *
     * @param Array $recipients
     * @param  Array $data
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        /*
         *  use this inside a controller to call this job
         *

          Log::info("Adding Job...");
          $this->dispatch(new sampleJob());
          Log::info("Job Queued!...");

         *
         */
        Log::info('Handling job...');

        Log::info('Eng processing of job...');
    }

}
