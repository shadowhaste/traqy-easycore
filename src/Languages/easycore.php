<?php

return [
	"crud" => [
		"create" => "%item% successfully created",
		"update" => "%item% successfully updated",
		"delete" => "%item% successfully deleted",
		"profile" => "Profile successfully updated",
		"update_password" => "Password successfully updated",
		"reset_password" => "User password successfully reset",
	],
];