<?php

return [
    "app_name" => "Traqy Package",
    "system" => [
        "name" => "Aries",
        "email" => "ariestraquena@gmail.com",
    ],
    "system_modules" => [
        'users',
        'roles',
        'permissions',
        'settings'
    ],
    "default_user" => "Administrator"
];
