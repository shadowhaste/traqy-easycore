<?php

namespace Traqy\EasyCore\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
    	return view( "easyCore::pages.home" );
    }
}
