<?php

namespace Traqy\EasyCore\Http\Controllers;

use Illuminate\Http\Request;
use Traqy\EasyCore\Interfaces\UserServiceInterface;
use Traqy\EasyCore\Interfaces\GuiServiceInterface;
use Traqy\EasyCore\Interfaces\DatatableInterface;
use Traqy\EasyCore\Http\Requests\Users\CreateRequest;
use Traqy\EasyCore\Http\Requests\Users\UpdateRequest;
use Traqy\EasyCore\Helpers\Helper;
use Traqy\EasyCore\Interfaces\UserRoleServiceInterface;
use Traqy\EasyCore\Interfaces\RoleServiceInterface;

class UsersController extends CoreController implements DatatableInterface {

    protected $roleService;
    protected $userRoleService;

    public function __construct(GuiServiceInterface $service, UserServiceInterface $moduleService, RoleServiceInterface $roleService, UserRoleServiceInterface $userRoleService) {
        parent::__construct($service, $moduleService, "users", "/users/record");

        $this->roleService = $roleService;
        $this->userRoleService = $userRoleService;

        $this->service->setTitle("User");

        $this->service->addColumn("ID", "id");
        $this->service->addColumn("Name", "name");
        $this->service->addColumn("Email", "email");
        $this->service->addColumn("Created", "created_at");

        $this->service->addField("textfield", [
            "key" => "name",
            "label" => "Name",
            "placeholder" => "Enter Name",
        ]);

        $this->service->addField("textfield", [
            "key" => "email",
            "label" => "Email Address",
            "placeholder" => "Enter Email Address",
        ]);

        $this->service->addField("user-checkbox", [
            "key" => "role_ids",
            "label" => "Roles",
            "placeholder" => "Select Roles",
            "choices" => $this->roleService->choices()
        ]);
    }

    public function records(Request $request) {
        $columns = $this->service->getColumnFields(false);

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $userId = auth()->user()->id;
        $records = $this->service->model->getUsersExcludeCurrent($userId, $columns);

        $totalData = $records->count();
        $totalFiltered = $totalData;

        if (empty($request->input('search.value'))) {
            $records = $this->service->model->searchUserDatatableByValue($userId, $start, $limit, $order, $dir, $columns);
        } else {

            $search = $request->input('search.value');
            $records = $this->service->model->searchUserDatatable($userId, $search, $start, $limit, $order, $dir, $columns);
            $totalFiltered = $this->service->model->searchUserDatatableCount($userId, $search);
        }

        return $this->displayRecords($records, $request, $totalData, $totalFiltered);
    }

    public function displayRecords($users, $request, $totalData, $totalFiltered) {
        $data = array();
        if (!empty($users)) {
            foreach ($users as $user) {
                $canEdit = Helper::isAllowed(auth()->user()->id, $this->route, 'perm_edit');
                $editMessage = ($canEdit) ? "Edit" : "No permission to edit";

                $show = route('users.show', $user->id);
                $edit = ($canEdit) ? route('users.edit', $user->id) : "#";

                $nestedData['id'] = $user->id;
                $nestedData['name'] = $user->name;
                $nestedData['email'] = $user->email;
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($user->created_at));
                $nestedData['options'] = "&emsp;<a href='{$show}' title='Show' ><span class='glyphicon glyphicon-list'></span></a>
                                          &emsp;<a href='{$edit}' title='{$editMessage}' ><span class='glyphicon glyphicon-edit'></span></a>";
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }

    public function create() {
        return view("easyCore::partials.create", [
            "service" => $this->service
        ]);
    }

    public function store(CreateRequest $request) {
        $user = $this->service->model->create($request->all());
        if ($user != null) {
            $this->userRoleService->assignRoles($user->id, $request->role_ids);
        }

        return redirect($this->service->route("list"))->with("status", $this->service->message("create"));
    }

    public function edit($id) {
        return view("easyCore::partials.users.edit", [
            "service" => $this->service,
            "userData" => $this->service->model->read($id),
            "data" => $this->userRoleService->getRoles($id)
        ]);
    }

    public function update(UpdateRequest $request, $id) {
        return $this->doUpdate($request, $id);
    }

    public function doUpdate(Request $request, $id) {
        $update = $this->service->model->update([
            "name" => $request->name,
                ], $request->id);

        if ($update) {
            $this->userRoleService->assignRoles($id, $request->role_ids);
        }

        return redirect($this->service->route("list"))->with("status", $this->service->message("update"));
    }

    public function profile() {
        $currentUser = auth()->user();
        $this->service->setTitle("User Profile");
        return view("easyCore::partials.users.profile", [
            "service" => $this->service,
            "canEdit" => Helper::isAllowed($currentUser->id, $this->route, 'perm_edit'),
            "user_data" => $currentUser
        ]);
    }

    public function updateProfile(Request $request) {
        $update = $this->service->model->update([
            "name" => $request->name,
                ], $request->id);
        return response()->json([
                    'message' => __('messages.user.profile.updated'),
                        ], 200);
    }

}
