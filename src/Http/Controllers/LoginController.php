<?php

namespace Traqy\EasyCore\Http\Controllers;

use Traqy\EasyCore\Interfaces\GuiServiceInterface;
use Traqy\EasyCore\Interfaces\UserServiceInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LoginController extends CoreController {

    public function __construct(GuiServiceInterface $service, UserServiceInterface $moduleService) {
        parent::__construct($service, $moduleService, "login");
    }

    public function get() {

        if (Auth::check()) {
            redirect("/");
        }
        return view(
                "easyCore::pages.login");
    }

    public function login(Request $request) {
        $rules = array(
            'email' => 'required|email',
            'password' => 'required'
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'data' => $validator->errors()
            ]);
        }

        $userdata = array(
            'email' => $request->email,
            'password' => $request->password
        );

        if (Auth::attempt($userdata)) {
            return redirect('/home');
        }
    }

    public function logout() {
        Auth::logout();
        return redirect("login");
    }

}
