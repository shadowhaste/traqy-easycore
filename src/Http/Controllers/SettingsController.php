<?php

namespace Traqy\EasyCore\Http\Controllers;

use Traqy\EasyCore\Interfaces\GuiServiceInterface;
use Traqy\EasyCore\Interfaces\UserServiceInterface;
use Traqy\EasyCore\Http\Requests\Settings\UpdatePasswordRequest;
use Traqy\EasyCore\Helpers\Helper;

class SettingsController extends CoreController {

    public function __construct(GuiServiceInterface $service, UserServiceInterface $moduleService) {
        parent::__construct($service, $moduleService, "settings", "/settings");
        $this->service->setTitle("Settings");
    }

    public function index() {
        return view("easyCore::partials.settings.index", [
            "service" => $this->service,
            "canEdit" => Helper::isAllowed(auth()->user()->id, $this->route, 'perm_edit')
        ]);
    }

    public function updatePassword(UpdatePasswordRequest $request) {
        $result = $this->moduleService->updatePassword($request);
        return response()->json([
                    'message' => $result->message
                        ], $result->status);
    }

}
