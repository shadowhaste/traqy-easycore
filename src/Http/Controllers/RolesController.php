<?php

namespace Traqy\EasyCore\Http\Controllers;

use Traqy\EasyCore\Interfaces\RoleServiceInterface;
use Traqy\EasyCore\Interfaces\GuiServiceInterface;
use Traqy\EasyCore\Interfaces\DatatableInterface;
use Traqy\EasyCore\Http\Requests\Roles\CreateRequest;
use Traqy\EasyCore\Helpers\Helper;

class RolesController extends CoreController implements DatatableInterface {

    public function __construct(GuiServiceInterface $service, RoleServiceInterface $moduleService) {
        parent::__construct($service, $moduleService, "roles", "/roles/record");

        $this->service->setTitle("Role");

        $this->service->addColumn("ID", "id");
        $this->service->addColumn("Name", "name");
        $this->service->addColumn("Created", "created_at");

        $this->service->addField("textfield", [
            "key" => "name",
            "label" => "Name",
            "placeholder" => "Enter Name",
        ]);
    }

    public function displayRecords($roles, $request, $totalData, $totalFiltered) {
        $data = array();
        if (!empty($roles)) {
            foreach ($roles as $role) {
                $canViewPermission = Helper::isAllowed(auth()->user()->id, 'permissions', 'perm_view');

                $permission = ( $canViewPermission ) ? route('roles.permission', $role->id) : "#";
                $permissionMessage = ( $canViewPermission ) ? "View Permissions" : "Not permitted to view permissions";

                $nestedData['id'] = $role->id;
                $nestedData['name'] = $role->name;
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($role->created_at));
                $nestedData['options'] = "&emsp;<a href='{$permission}' title='{$permissionMessage}' ><span class='glyphicon glyphicon-lock'></span></a>";
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }

    public function store(CreateRequest $request) {
        return parent::doStore($request);
    }

}
