<?php

namespace Traqy\EasyCore\Http\Controllers;

use Traqy\EasyCore\Interfaces\PermissionServiceInterface;
use Traqy\EasyCore\Interfaces\RoleServiceInterface;
use Traqy\EasyCore\Interfaces\GuiServiceInterface;
use Traqy\EasyCore\Interfaces\DatatableInterface;
use Illuminate\Http\Request;
use Traqy\EasyCore\Http\Requests\Permissions\CreateRequest;

class PermissionsController extends CoreController implements DatatableInterface {

    protected $roleService;

    public function __construct(GuiServiceInterface $service, PermissionServiceInterface $moduleService, RoleServiceInterface $roleService) {
        parent::__construct($service, $moduleService, "permissions", "/permissions/record");

        $this->service->setTitle("Permission");
        $this->service->addColumn("ID", "id");
        $this->service->addColumn("MODULE", "module_name");
        $this->service->addColumn("ADD", "perm_add");
        $this->service->addColumn("EDIT", "perm_edit");
        $this->service->addColumn("VIEW", "perm_view");
        $this->service->addColumn("DELETE", "perm_delete");
        $this->service->addColumn("Created", "created_at");
        $this->roleService = $roleService;
    }

    public function showPermissions($roleID) {
        $records = $this->service->model->findAllByOrderBy('role_id', $roleID, ['*'], 'module_name');
        $roleObj = $this->roleService->read($roleID);

        return view("easyCore::partials.permissions.record", [
            "service" => $this->service,
            "record" => $records,
            "id" => $roleID,
            "role" => $roleObj->name
        ]);
    }

    public function records(Request $request) {
        $roleID = $request->id;
        $columns = $this->service->getColumnFields(false);

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $records = $this->service->model->findAllByOrderBy('role_id', $roleID, ['*'], 'module_name');

        $totalData = $records->count();
        $totalFiltered = $totalData;

        if (empty($request->input('search.value'))) {
            $records = $this->service->model->searchDatatableByValue($start, $limit, $order, $dir, $columns, $roleID);
        } else {

            $search = $request->input('search.value');
            $records = $this->service->model->searchDatatable($search, $start, $limit, $order, $dir, $columns, $roleID);
            $totalFiltered = $this->service->model->searchDatatableCount($search, $roleID);
        }

        return $this->displayRecords($records, $request, $totalData, $totalFiltered);
    }

    public function displayRecords($permissions, $request, $totalData, $totalFiltered) {
        $data = array();
        if (!empty($permissions)) {
            foreach ($permissions as $permission) {

                $nestedData['id'] = $permission->id;
                $nestedData['module_name'] = $permission->module_name;

                $nestedData['perm_add'] = "<input type='checkbox' "
                        . "value='{$permission->perm_add}'"
                        . "class='chk-permissions' data-id='{$permission->id}'"
                        . "data-permission='perm_add' {$this->checked($permission->perm_add)}>";


                $nestedData['perm_edit'] = "<input type='checkbox' "
                        . "value='{$permission->perm_edit}'"
                        . "class='chk-permissions' data-id='{$permission->id}'"
                        . "data-permission='perm_edit' {$this->checked($permission->perm_edit)}>";


                $nestedData['perm_view'] = "<input type='checkbox' "
                        . "value='{$permission->perm_view}'"
                        . "class='chk-permissions' data-id='{$permission->id}'"
                        . "data-permission='perm_view' {$this->checked($permission->perm_view)}>";


                $nestedData['perm_delete'] = "<input type='checkbox' "
                        . "value='{$permission->perm_delete}'"
                        . "class='chk-permissions' data-id='{$permission->id}'"
                        . "data-permission='perm_delete' {$this->checked($permission->perm_delete)}>";

                $nestedData['created_at'] = date('j M Y h:i a', strtotime($permission->created_at));

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }

    private function checked($value) {
        return ($value > 0) ? "checked" : "";
    }

    public function createPermission($roleID) {
        $this->service->addField("select", [
            "key" => "module_name",
            "label" => "Modules",
            "placeholder" => "Select Modules",
            "choices" => \Traqy\EasyCore\Helpers\Helper::getmodules()
        ]);

        $this->service->addField("checkbox", [
            "key" => "permission_type",
            "label" => "Permissions",
            "placeholder" => "Select Modules",
            "choices" => \Traqy\EasyCore\Helpers\Helper::getPermissionTypes()
        ]);

        $roleObj = $this->roleService->read($roleID);

        return view("easyCore::partials.permissions.create", [
            "service" => $this->service,
            "role_id" => $roleID,
            "role" => $roleObj->name
        ]);
    }

    public function store(CreateRequest $request) {
        $data = array();
        $data['role_id'] = $request->role_id;
        $data['module_name'] = $request->module_name;

        $data['perm_add'] = 0;
        $data['perm_edit'] = 0;
        $data['perm_view'] = 0;
        $data['perm_delete'] = 0;

        if (!empty($request->permission_type)) {
            foreach ($request->permission_type as $type) {
                $data[$type] = 1;
            }
        }

        $this->service->model->create($data);

        return redirect(route('roles.store.permission', $request->role_id))->with("status", $this->service->message("create"));
    }

    public function updatePermission(Request $request) {
        try {
            $this->service->model->update([$request->permission => $request->value], $request->id);
            return response()->json(array('success' => true));
        } catch (Exception $e) {
            return response()->json(array('success' => false, 'message' => $e->getMessage()));
        }

        return response()->json(array('success' => false, 'message' => 'something_went_wrong'));
    }

}
