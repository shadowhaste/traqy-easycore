<?php

namespace Traqy\EasyCore\Http\Controllers;

use Traqy\EasyCore\Interfaces\GuiServiceInterface;
use Traqy\EasyCore\Interfaces\ServiceInterface;
use Traqy\EasyCore\Helpers\Helper;
use Illuminate\Http\Request;

class CoreController extends Controller {

    protected $service;
    protected $moduleService;
    protected $repository;
    protected $route;

    public function __construct(GuiServiceInterface $service, ServiceInterface $moduleService, $route, $recordRoute = null) {
        $this->route = $route;
        $this->service = $service;
        $this->moduleService = $moduleService;
        $this->service->setModel($moduleService->getRepository());
        $this->service->setRoute($route);
        $this->service->setRecordRoute($recordRoute);
    }

    public function index() {
        $records = $this->service->model->getAllOrderBy();

        return view("easyCore::partials.record", [
            "service" => $this->service,
            "record" => $records,
            "canAdd" => Helper::isAllowed(auth()->user()->id, $this->route, 'perm_add')
        ]);
    }

    public function records(Request $request) {
        $columns = $this->service->getColumnFields(false);

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $records = $this->service->model->getAll($columns);

        $totalData = $records->count();
        $totalFiltered = $totalData;

        if (empty($request->input('search.value'))) {
            $records = $this->service->model->searchDatatableByValue($start, $limit, $order, $dir, $columns);
        } else {

            $search = $request->input('search.value');
            $records = $this->service->model->searchDatatable($search, $start, $limit, $order, $dir, $columns);
            $totalFiltered = $this->service->model->searchDatatableCount($search);
        }

        return $this->displayRecords($records, $request, $totalData, $totalFiltered);
    }

    public function displayRecords($records, $request, $totalData, $totalFiltered) {
        #this function meant to be overidden
        return null;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view("easyCore::partials.create", [
            "service" => $this->service
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function doStore(Request $request) {
        $this->service->model->create($request->all());
        return redirect($this->service->route("list"))->with("status", $this->service->message("create"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        return view("easyCore::partials.edit", ["service" => $this->service, "data" => $this->service->model->read($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function doUpdate(Request $request, $id) {
        $this->service->model->updateByFields($this->service->fields, $id, $request);
        return redirect($this->service->route("list"))->with("status", $this->service->message("update"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
