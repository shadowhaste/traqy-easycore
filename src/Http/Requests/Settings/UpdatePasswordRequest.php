<?php

namespace Traqy\EasyCore\Http\Requests\Settings;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class UpdatePasswordRequest extends FormRequest {

    public function authorize() {
        return Auth::check();
    }

    public function rules() {
        return [
            "oldpassword" => "required",
            "password" => "required",
            "confirm_password" => "required",
        ];
    }

    public function messages() {
        return [
            "oldpassword.required" => "Old password is required",
            "password.required" => "New password is required",
            "confirm_password.required" => "Confirm password is required",
        ];
    }

}
