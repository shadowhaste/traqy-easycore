<?php

namespace Traqy\EasyCore\Http\Requests\Roles;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|unique:roles,name",
        ];
    }

    public function messages(){
        return [
            "name.unique" => "Name already exist",
            "name.required" => "Name is required",
        ];
    }
}
