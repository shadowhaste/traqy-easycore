<?php

namespace Traqy\EasyCore\Http\Requests\Permissions;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class CreateRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            "module_name" => "required|is_module_exist",
        ];
    }

    public function messages() {
        return [
            "module_name.required" => "Module is required",
            "module_name.is_module_exist" => "Module already recorded",
        ];
    }

}
