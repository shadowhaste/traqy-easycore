<?php

namespace Traqy\EasyCore\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class CreateRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            "email" => "required|email|unique:users,email",
            "name" => "required",
            "role_ids" => "required",
        ];
    }

    public function messages() {
        return [
            "email.required" => "Email is required",
            "email.email" => "Provide a valid email address",
            "email.unique" => "Email already exist",
            "name.required" => "Name is required",
            "role_ids.required" => "Role is required",
        ];
    }

}
