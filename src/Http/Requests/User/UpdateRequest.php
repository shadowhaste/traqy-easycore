<?php

namespace Traqy\EasyCore\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use Illuminate\Http\Request;

class UpdateRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request) {
        return [
            "email" => "required|unique_user_on_update:{$request->id},email,{$request->id},id,deleted_at,NULL|email",
            "name" => "required",
            "role_ids" => "required",
        ];
    }

    public function messages() {
        return [
            "email.required" => "Email is required",
            "email.email" => "Provide a valid email address",
//            "email.unique" => "Email already exist",
            "name.required" => "Name is required",
            "role_ids.required" => "Role is required",
        ];
    }

}
