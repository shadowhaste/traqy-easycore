<?php

namespace Traqy\EasyCore\Fields;

use Traqy\EasyCore\Interfaces\FieldInterface;

class SelectField extends CoreField implements FieldInterface{
	protected $placeholder;
	protected $choices;

	public function __construct( $args = [] ){
		parent::__construct( $args );
		$this->placeholder = $args->placeholder;
		$this->choices = $args->choices;
	}

	public function getPlaceholder(){
		return $this->placeholder;
	}

	public function getChoices(){
		return $this->choices;
	}

	public function render( $data = false ){
		return $this->view("easyCore::fields.select-field", $data);
	}

	public function selected( $data = false, $compare ){
		return $this->getValue( $data ) == $compare ? "selected" : "";
	}


}