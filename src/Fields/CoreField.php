<?php

namespace Traqy\EasyCore\Fields;

class CoreField{

	protected $key;
	protected $label;

	public function __construct( $args = [] ){
		$this->key = $args->key;
		$this->label = $args->label;
	}

	public function getKey( $key = false ){
		return $key ? $this->key . ".{$key}" : $this->key;
	}

	public function getValue( $data = false ){
		$old = old( $this->key );
		if( $old ) return $old;

		$key = $this->key;

		return $data ? $data->$key : "";
	}

	public function getLabel(){
		return $this->label;
	}

	public function hasError( $error ){
		return $error->has( $this->key ) ? "has-error" : "";
	}

	public function view( $view, $data ){
		return view( $view, [ "field" => $this, "data" => $data ] );
	}

	public function input( $request ){
		return $request->input( $this->key );
	}
}