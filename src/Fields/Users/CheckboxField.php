<?php

namespace Traqy\EasyCore\Fields\Users;
use Traqy\EasyCore\Fields\CoreField;
use Traqy\EasyCore\Interfaces\FieldInterface;

class CheckboxField extends CoreField implements FieldInterface{
	protected $choices;

	public function __construct( $args = [] ){
		parent::__construct( $args );
		$this->choices = $args->choices;
	}

	public function getChoices(){
		return $this->choices;
	}

	public function render( $data = false ){
		return $this->view("easyCore::fields.checkbox-field", $data);
	}

	public function checked( $data = false, $id ){
		return $data && in_array( $id, $data ) ? "checked" : "";
	}
}