<?php

namespace Traqy\EasyCore\Fields;

use Traqy\EasyCore\Interfaces\FieldInterface;

class PasswordField extends CoreField implements FieldInterface{

	protected $placeholder;

	public function __construct( $args = [] ){
		parent::__construct( $args );
		$this->placeholder = $args->placeholder;
	}

	public function getPlaceholder(){
		return $this->placeholder;
	}

	public function render( $data = false ){
		return $this->view("easyCore::fields.password-field", $data);
	}
}