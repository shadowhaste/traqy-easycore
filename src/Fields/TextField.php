<?php

namespace Traqy\EasyCore\Fields;

use Traqy\EasyCore\Interfaces\FieldInterface;

class TextField extends CoreField implements FieldInterface{

	protected $placeholder;
	protected $disabled;

	public function __construct( $args = [] ){
		parent::__construct( $args );
		$this->placeholder = $args->placeholder;
		if (isset($args->disabled)) {
			$this->disabled = $args->disabled;
		}else{
			$this->disabled = 0;
		}
		if (isset($args->limit)) {
			$this->limit = $args->limit;
		}else{
			$this->limit = 0;
		}

	}

	public function getPlaceholder(){
		return $this->placeholder;
	}

	public function getDisabled(){
		return $this->disabled;
	}

	public function getLimit(){
		return $this->limit;
	}

	public function render( $data = false ){
		return $this->view("easyCore::fields.text-field", $data);
	}
}