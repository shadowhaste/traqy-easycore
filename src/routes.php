<?php

Route::group(["middleware" => "web"], function() {

    Route::group(["middleware" => "guest"], function() {

        Route::get('login', 'Traqy\EasyCore\Http\Controllers\LoginController@get')->name('login');
        Route::post('login', ['as' => 'login', 'uses' => 'Traqy\EasyCore\Http\Controllers\LoginController@login']);
    });

    Route::group(["middleware" => "auth"], function() {

        Route::get('/', "Traqy\EasyCore\Http\Controllers\HomeController@index");
        Route::get('/home', "Traqy\EasyCore\Http\Controllers\HomeController@index");
        Route::get('logout', 'Traqy\EasyCore\Http\Controllers\LoginController@logout');

        Route::resource('users', 'Traqy\EasyCore\Http\Controllers\UsersController');
        Route::post('/users/record', "Traqy\EasyCore\Http\Controllers\UsersController@records");

        Route::resource('roles', 'Traqy\EasyCore\Http\Controllers\RolesController');
        Route::post('/roles/record', "Traqy\EasyCore\Http\Controllers\RolesController@records");


        Route::resource('permissions', 'Traqy\EasyCore\Http\Controllers\PermissionsController');
        Route::get('/permissions/show/{role}', "Traqy\EasyCore\Http\Controllers\PermissionsController@showPermissions")->name('roles.permission');
        Route::post('/permissions/record', "Traqy\EasyCore\Http\Controllers\PermissionsController@records");
        Route::post('/permissions/updatePermission', "Traqy\EasyCore\Http\Controllers\PermissionsController@updatePermission");
        Route::get('/permissions/create/{role_id}', "Traqy\EasyCore\Http\Controllers\PermissionsController@createPermission")->name('roles.create.permission');
        Route::post('/permissions/show/{role}', "Traqy\EasyCore\Http\Controllers\PermissionsController@store")->name('roles.store.permission');


        Route::get('settings', 'Traqy\EasyCore\Http\Controllers\SettingsController@index');
        Route::post('/settings/updatePassword', "Traqy\EasyCore\Http\Controllers\SettingsController@updatePassword");

        Route::get('profile', 'Traqy\EasyCore\Http\Controllers\UsersController@profile');
        Route::post('profile/update', "Traqy\EasyCore\Http\Controllers\UsersController@updateProfile");
    });
});







