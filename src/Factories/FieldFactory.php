<?php

namespace Traqy\EasyCore\Factories;

use Traqy\EasyCore\Interfaces\FactoryInterface;

class FieldFactory implements FactoryInterface{

	public static function make( $type, $args = [] ){

		switch( $type ){
			case "checkbox":
				return new \Traqy\EasyCore\Fields\CheckboxField($args);
			case "user-checkbox":
				return new \Traqy\EasyCore\Fields\Users\CheckboxField($args);
//
//			case "image":
//				return new \Section8\CMS\Fields\ImageField($args);
//
//			case "radio":
//				return new \Section8\CMS\Fields\RadioField($args);
//
			case "select":
				return new \Traqy\EasyCore\Fields\SelectField($args);
//
//			case "textarea":
//				return new \Section8\CMS\Fields\TextareaField($args);
//
			case "textfield":
				return new \Traqy\EasyCore\Fields\TextField($args);
//
//			case "numberfield":
//				return new \Section8\CMS\Fields\NumberField($args);
//
			case "password":
				return new \Traqy\EasyCore\Fields\PasswordField($args);
//
//			case "password-change":
//				return new \Section8\CMS\Fields\PasswordChangeField($args);
//
//			case "hidden":
//				return new \Section8\CMS\Fields\HiddenField($args);
//
//			case "wysiwyg":
//				return new \Section8\CMS\Fields\WysiwygField($args);
//
//			case "date":
//				return new \Section8\CMS\Fields\DateField($args);

			default:
				return new $type( $args );
		}

	}

}