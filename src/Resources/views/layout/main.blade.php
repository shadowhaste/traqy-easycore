 <!DOCTYPE html>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="csrf-token" content="{{ csrf_token() }}">

		@if( isset( $service ) )
			<title>{{ config( "easyCore.app_name" ) }} | {{ $service->getTitle() }}</title>
		@else
			<title>{{ config( "easyCore.app_name" ) }}</title>
		@endif

		<!-- Bootstrap Core CSS -->
		<link href="/vendor/easycore/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

		<!-- MetisMenu CSS -->
		<link href="/vendor/easycore/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

		<!-- Custom CSS -->
		<link href="/vendor/easycore/dist/css/sb-admin-2.css" rel="stylesheet">

		<!-- Morris Charts CSS -->
		<link href="/vendor/easycore/vendor/morrisjs/morris.css" rel="stylesheet">

		<!-- Custom Fonts -->
		<link href="/vendor/easycore/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

		@stack('scripts-css')
	</head>

	<body>
		<div id="app">
			<div id="wrapper">
				<!-- Navigation -->
				<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
					@include("easyCore::layout.menu")
					@include("easyCore::layout.sidebar")
				</nav>

				 <div id="page-wrapper">
					@yield('content')
				 </div>

			</div>
		</div>


		<script src="/vendor/easycore/js/vuejs/vue.min.js"></script>
		<script src="/js/app.js"></script>
		<script src="/vendor/easycore/js/axios.min.js"></script>

		<!-- jQuery -->
		<script src="/vendor/easycore/vendor/jquery/jquery.min.js"></script>

		<!-- Bootstrap Core JavaScript -->
		<script src="/vendor/easycore/vendor/bootstrap/js/bootstrap.min.js"></script>

		<!-- Metis Menu Plugin JavaScript -->
		<script src="/vendor/easycore/vendor/metisMenu/metisMenu.min.js"></script>

		<!-- Morris Charts JavaScript -->
		<script src="/vendor/easycore/vendor/raphael/raphael.min.js"></script>
		<script src="/vendor/easycore/vendor/morrisjs/morris.min.js"></script>
		<script src="/vendor/easycore/data/morris-data.js"></script>

		<!-- Custom Theme JavaScript -->
		<script src="/vendor/easycore/dist/js/sb-admin-2.js"></script>

		@stack('scripts')
	</body>

</html>