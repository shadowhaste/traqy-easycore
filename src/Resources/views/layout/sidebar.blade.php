@section('sidebar')

 <div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse">
		<ul class="nav" id="side-menu">
			<li>
				<a href="/"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
			</li>
			<li>
				<a href="#"><i class="fa fa-users fa-fw"></i> User Management<span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
					<li>
						@if(Traqy\EasyCore\Helpers\Helper::isAllowed(Auth::user()->id, 'users', 'perm_view'))
							<a href="/users">Users</a>
						@endif
					</li>
					<li>
						@if(Traqy\EasyCore\Helpers\Helper::isAllowed(Auth::user()->id, 'roles', 'perm_view'))
							<a href="/roles">Roles</a>
						@endif

					</li>
				</ul>
				<!-- /.nav-second-level -->
			</li>
		</ul>
	</div>
	<!-- /.sidebar-collapse -->
</div>