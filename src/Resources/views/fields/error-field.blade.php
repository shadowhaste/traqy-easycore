@if( $errors->get( $field->getKey( isset($key) ? $key : false ) ) )
<div class="help-block">{{ $errors->first( $field->getKey( isset($key) ? $key : false ) ) }}</div>
@endif