<div class="form-group {{ $field->hasError( $errors ) }}">
	<label for="select-field-{{ $field->getKey() }}">{{ $field->getLabel() }}</label>
	<select name="{{ $field->getKey() }}" class="form-control" id="select-field-{{ $field->getKey() }}">
		<option value="">{{ $field->getPlaceholder() }}</option>
		@foreach( $field->getChoices() as $item )
		<option value="{{ $item->id }}" {{ $field->selected( $data, $item->id ) }}>{{ $item->name }}</option>
		@endforeach
	</select>
	@include( "easyCore::fields.error-field", [ "field" => $field ] )
</div>