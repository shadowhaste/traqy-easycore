<div class="form-group {{ $field->hasError( $errors ) }}">
	<label for="password-field-{{ $field->getKey() }}">{{ $field->getLabel() }}</label>
	<input
		type="password"
		class="form-control"
		id="password-field-{{ $field->getKey() }}"
		placeholder="{{ $field->getPlaceholder() }}"
		value=""
		name="{{ $field->getKey() }}"
	>

</div>