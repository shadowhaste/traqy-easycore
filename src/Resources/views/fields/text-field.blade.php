<div class="form-group {{ $field->hasError( $errors ) }}">
	<label for="text-field-{{ $field->getKey() }}">{{ $field->getLabel() }}</label>
	<input
		type="text"
		class="form-control"
		id="text-field-{{ $field->getKey() }}"
		placeholder="{{ $field->getPlaceholder() }}"
		value="{{ $field->getValue( $data ) }}"
		name="{{ $field->getKey() }}"
		@if($field->getDisabled() == 1) readonly @endif
		@if($field->getLimit() != 0) maxlength="{{ $field->getLimit() }}" @endif
	>
	@include( "easyCore::fields.error-field", [ "field" => $field ] )
</div>