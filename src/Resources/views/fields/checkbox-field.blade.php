<div class="form-group {{ $field->hasError( $errors ) }}">
	<label for="checkbox-field-{{ $field->getKey() }}">{{ $field->getLabel() }}</label>
	<div class="row">
		@foreach( $field->getChoices() as $item )
		<div class="col-md-3">
		<div class="checkbox">
			<label for="checkbox-field-{{ $field->getKey() }}-{{ $item->id }}">
			<input
				type="checkbox"
				id="checkbox-field-{{ $field->getKey() }}-{{ $item->id }}"
				value="{{ $item->id }}"
				name="{{ $field->getKey() }}[]"
				{{ $field->checked( $data, $item->id ) }}
			>
			{{ $item->name }}
			</label>
		</div>
		</div>
		@endforeach
	</div>
	@include( "easyCore::fields.error-field", [ "field" => $field ] )
</div>