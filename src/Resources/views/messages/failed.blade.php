@if( session( "failed" ) )
	<div class="alert alert-danger">
		<p>{{ session('failed') }}</p>
	</div>
@endif