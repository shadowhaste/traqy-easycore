@extends('easyCore::layout.main')

@section('content')

<div class="row">
	 <div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Add {{ strtolower($role) . ' ' . strtolower($service->getTitle()) }}</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</div>
</div>

<div class="row">

	<form method="POST" action="{{ route('roles.store.permission', $role_id) }}" enctype="multipart/form-data">
		<input type="hidden" name="role_id" value="{{ $role_id }}" />
		<div class="col-md-8">

				@include( "easyCore::partials.fields", [ "fields" => $service->fields, "data" => false ] )

		</div>
		<div class="col-md-8">
			<a class="btn btn-default" href="{{ route('roles.permission', $role_id) }}">Cancel</a>
			<button type="submit" class="btn btn-primary pull-right">Create {{ $service->getTitle() }}</button>
		</div>

	</form>

</div>
@endsection



