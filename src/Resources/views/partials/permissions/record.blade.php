@extends('easyCore::layout.main')
@include("easyCore::extra.datatable")

@section('content')

<div class="row">
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ ucfirst($role) . ' ' .str_plural( $service->getTitle() )  }}</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <a href="{{ $service->route('create') . '/' . $id}}" type="button" class="btn btn-primary">Add {{  $service->getTitle()  }}</a>
        <a class="btn btn-default" href="{{ route('roles.index') }}">Back</a>
        <br /><br />
    </div>

    <div class="col-md-8">
        @include("easyCore::messages.success")
        {{-- @include("easyCore::messages.errors") --}}
        @include("easyCore::messages.failed")
    </div>

    <div class="col-md-12">

        <table class="table table-bordered" id="{{ strtolower(str_plural( $service->getTitle() ))  }}">
            <thead>
                @foreach($service->getColumnLabels(false) as $item)
            <th>{{ $item }}</th>
            @endforeach
            </thead>
        </table>

        <input type="hidden" id="inp-table" value="{{ strtolower(str_plural( $service->getTitle() ))  }}"/>
        <input type="hidden" id="inp-routes" value="{{ $service->getRecordRoute() }}"/>
        <input type="hidden" id="inp-columns" value="{{  json_encode($service->getDatatableColumns(false))  }}"/>

        @if(isset($id))
        <input type="hidden" id="inp-id" value="{{ $id }}"/>
        @endif

        <permission-component></permission-component>
    </div>

</div>
@endsection




