@extends('easyCore::layout.main')
@include("easyCore::extra.datatable")

@section('content')

<div class="row"> 
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ str_plural( $service->getTitle() )  }}</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
</div>

<div class="row">
    @if( $canAdd )

    <div class="col-md-8">
        <a href="{{ $service->route('create')}}" type="button" class="btn btn-primary">Add {{  $service->getTitle()  }}</a>
        <br /><br />
    </div>

    <div class="col-md-8">
        @include("easyCore::messages.success")
        {{-- @include("easyCore::messages.errors") --}}
        @include("easyCore::messages.failed")
    </div>

    @endif


    <div class="col-md-8">
        <table class="table table-bordered" id="{{ strtolower(str_plural( $service->getTitle() ))  }}">
            <thead>
                @foreach($service->getColumnFields() as $item)
            <th>{{ ucfirst($item) }}</th>
            @endforeach
            </thead>
        </table>

        <input type="hidden" id="inp-table" value="{{ strtolower(str_plural( $service->getTitle() ))  }}"/>
        <input type="hidden" id="inp-routes" value="{{ $service->getRecordRoute() }}"/>
        <input type="hidden" id="inp-columns" value="{{  json_encode($service->getDatatableColumns())  }}"/>

        @if(isset($id))
        <input type="hidden" id="inp-id" value="{{ $id }}"/>
        @endif

    </div>
</div>
@endsection



