@extends('easyCore::layout.main')

@section('content')

<div class="row">
	 <div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Edit {{ $service->getTitle() }}</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</div>
</div>

<div class="row">

	<form method="POST" action="{{ $service->route("update", $userData->id) }}" enctype="multipart/form-data">
		{{ method_field('PATCH') }}
		<input type="hidden" name="id" value="{{ $userData->id }}" />
		<div class="col-md-8">

				@include( "easyCore::partials.users.fields",
				[
					"fields" => $service->fields,
					"userdata" => $userData,
					"data" => $data,

				] )

		</div>
		<div class="col-md-8">
			<a class="btn btn-default" href="{{ $service->route('list') }}">Cancel</a>
			<button type="submit" class="btn btn-primary pull-right">Update {{ $service->getTitle() }}</button>
		</div>

	</form>

</div>
@endsection



