@extends('easyCore::layout.main')

@section('content')

<div class="row">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{  $service->getTitle()  }}</h1> 
            </div> 
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        @if( $canEdit ) 
        <user-profile :user_data="{{ $user_data }}"></user-profile> 
        @endif
    </div>
</div>
@endsection



