@extends('easyCore::layout.main')
@include("easyCore::extra.datatable")

@section('content')

<div class="row">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ str_plural( $service->getTitle() )  }}</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
</div>

<div class="row">
    <div class="col-md-4">
    @if( $canEdit ) 
        <change-password-component></change-password-component>
    @endif
    </div>
</div>
@endsection



