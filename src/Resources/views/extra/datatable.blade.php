@push('scripts-css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css"/>

@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>

<script>
    $(document).ready(function() {
        var table = $('#inp-table').val();
        var route = $('#inp-routes').val();
        var dataColumns = $('#inp-columns').val();
        var idParam = $('#inp-id').val();

        $('#' + table).DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": route,
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}", id: idParam},
            },
            "columns": eval(dataColumns)

        });
    });
</script>
@endpush