import api from "../api";

export default {

    updatePassword(detail) {
        return new Promise((resolve, reject) => {
            api.post(
                "settings/updatePassword",
                detail, 
            )
                .then(response => {
                    return resolve(response);
                })
                .catch((error) => {   
                    return reject(error);
                });
        });
    },   

}
