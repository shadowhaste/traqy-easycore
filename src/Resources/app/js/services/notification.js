import Vue from './../app.js';
import swal from 'sweetalert';

export default {
    success(message) 
    {
        new Noty({                       
            type: 'success',                        
            layout: 'topRight',                     
            theme: 'semanticui',                        
            text: message,
            timeout: 3000         
        }).show();
    },
    error()
    {
        new Noty({                     
            type: 'error',                      
            layout: 'topRight',                     
            theme: 'semanticui',                        
            text: message,
            timeout: 3000                
        }).show();
    },
    successModal(message)
    {
        swal("Good job!", message, "success");
    },
    errorModal(message)
    {
        swal("Oh no!", message, "error");
    }
}