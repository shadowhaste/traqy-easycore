import api from "../api";

export default {

    updateUser(detail) {
        return new Promise((resolve, reject) => {
            api.post(
                "profile/update",
                detail, 
            ).then(response => {
                return resolve(response);
            })
            .catch((error) => {   
                return reject(error);
            });
        });
    },   

}
