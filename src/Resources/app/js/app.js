import BootstrapVue from "bootstrap-vue";
import store from "./store";
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

Vue.config.ignoredElements
        = [
            'merchant-header'
        ];

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Aries
//Vue.component('example-component', require('./components/ExampleComponent.vue'));

Vue.use(BootstrapVue);
// import 'bootstrap/dist/css/bootstrap.css';
// import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.component('permission-component', require('./components/PermissionComponent.vue'));
Vue.component('change-password-component', require('./components/ChangePasswordComponent.vue'));
Vue.component('user-profile', require('./components/UserProfileComponent.vue'));
const app = new Vue({
    el: '#app',
    store
});

 