import settingsService from "../services/settings"; 
import notification from '../services/notification.js';

export default {

    namespaced: true,
    state: {
        loading: false,
        errors: null,
        detail: {
            oldpassword: null,
            password: null,
            confirm_password: null
        }
    },

    mutations: {
        loading(state) {
            state.loading = true;
        },

        errors(state, errors) {
            state.errors = errors;
            state.loading = false;
        },

        setDetails(state, value) {
            state.detail.oldpassword = value.oldPassword;
            state.detail.password = value.newPassword;
            state.detail.confirm_password = value.confirmPassword;
        },  
    }, 

    actions: {
        updateDetails: async ({ commit, state }) => {
            commit("loading");

            try {
                let response = await settingsService.updatePassword(
                    state.detail,
                    state.errors = null
                ); 
                notification.successModal(response.message); 
            } catch (errors) {   
                commit("errors", errors); 
            }
        }
    }, 

}