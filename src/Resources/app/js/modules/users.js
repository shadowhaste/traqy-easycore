import userService from "../services/users.js"; 
import notification from '../services/notification.js';

export default {

    namespaced: true,
    state: {
        loading: false,
        errors: null,
        detail: {
            name: null, 
            id: null
        }
    },

    mutations: {
        loading(state) {
            state.loading = true;
        },

        errors(state, errors) {
            state.errors = errors;
            state.loading = false;
        },

        setDetails(state, value) {
            state.detail.name = value.name; 
            state.detail.id = value.id; 
        },  
    }, 

    actions: {
        updateDetails: async ({ commit, state }) => {
            commit("loading");

            try { 
                let response = await userService.updateUser(
                    state.detail,
                    state.errors = null
                );  
                notification.successModal(response.message); 
            } catch (errors) {  
                notification.errorModal(errors.errors.message);
                commit("errors", errors); 
            }
        }
    }, 

}